package ClientPackage;

/**
 * Student Name: Rui Wang
 * Student ID: 978296
 * Subject: Distributed System
 */


import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controller implements Initializable {
    private Client client;
    @FXML
    private Button Search;
    @FXML
    private Button Insert;
    @FXML
    private Button Remove;
    @FXML
    private TextField SearchInput;
    @FXML
    private TextField InsertWordInput;
    @FXML
    private TextField RemoveInput;
    @FXML
    private TextArea SearchOutput;
    @FXML
    private TextArea InsertMeaningInput;
    @FXML
    private TextArea InsertOutput;
    @FXML
    private TextArea RemoveOutput;
    @FXML
    private CheckBox lowercase;
    @FXML
    private TextField newPort;
    @FXML
    private TextField newHost;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        client = new Client();
        client.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!client.ConnectionError.equals(""))
            new Alert().display(client.ConnectionError,client.ConnectionErrorInfo);
        else if (!client.ConnectionEstablished){
            new Alert().display("Timeout Error","The server might be busy. Please retry later");
        }
        newHost.setText(Constants.getHost());
        newPort.setText(Integer.toString(Constants.getPort()));
    }

    public void sendSearch(ActionEvent event){
        if (lowercase.isSelected()){
            SearchInput.setText(SearchInput.getText().toLowerCase());
        }
        if(SearchInput.getText().equals("")){
            new Alert().display("Error","Please input the word");
        }
        else try{
            SearchOutput.setText(client.search(SearchInput.getText()));
        }
        catch (Exception e){
            new Alert().display("Error","Unable to connect the server.");
        }
    }

    public void sendRemove(ActionEvent event) {
        if (lowercase.isSelected()){
            RemoveInput.setText(RemoveInput.getText().toLowerCase());
        }
        if(RemoveInput.getText().equals("")){
            new Alert().display("Error","Please input the word");
        }
        else try{
            RemoveOutput.setText(client.remove(RemoveInput.getText()));
        }
        catch (Exception e){
            new Alert().display("Error","Unable to connect the server.");
        }

    }

    public void sendInsert(ActionEvent event) {
        if (lowercase.isSelected()){
            InsertWordInput.setText(InsertWordInput.getText().toLowerCase());
        }
        if(InsertMeaningInput.getText().equals("")){
            new Alert().display("Error","Please input the meaning");
        }
        else try{
            new Alert().display("Feedback",client.insert(InsertWordInput.getText(),InsertMeaningInput.getText()));
        }
        catch (Exception e){
            new Alert().display("Error","Unable to connect the server.");
        }
    }

    public void alllowercase(ActionEvent event){
        client.lowercase = lowercase.isSelected();
        if (lowercase.isSelected())
            new Alert().display("Success","All input will be transformed into lowercase.");
    }

    public void setNewPort(ActionEvent event){
        String p = newPort.getText();
        try {
            int value = Integer.valueOf(p);
            if (value<0||value>=65536)
                throw new Exception();
        }catch(Exception e){
            new Alert().display("Error", "Invalid Port Input");
            return;
        }
        new Alert().display("Success", "Change saved. Restart to apply the new configure.");
    }

    public void setNewHost(ActionEvent event){
        String p = newHost.getText();
        String regex = "(^((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})(\\.((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})){3}$)|(^localhost$)";
        if (!Pattern.matches(regex,p)){
            new Alert().display("Error", "Invalid Host Input");
        }
        else new Alert().display("Success", "Change saved. Restart to apply the new configure.");
    }

    public void exit(){
        Constants.saveAndExit();
        client.exit();
        System.exit(0);
    }
}