package ClientPackage;

import javafx.scene.control.TextField;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Student Name: Rui Wang
 * Student ID: 978296
 * Subject: Distributed System
 */


public class Client extends Thread{
    private String ip;
    private int port;
    private DataInputStream is;
    private DataOutputStream os;
    public String ConnectionError;
    public String ConnectionErrorInfo;
    public boolean lowercase = false;
    public boolean ConnectionEstablished;
    public Client (){
        Constants.recoverFromFile();
        ip = Constants.getHost();
        port = Constants.getPort();
        ConnectionError = "";
        ConnectionErrorInfo = "";
        ConnectionEstablished = false;
    }

    public void run(){
        try (Socket socket = new Socket(ip, port);){
            socket.setSoTimeout(1000);
            // Output and Input Stream
            is = new DataInputStream(socket.getInputStream());
            os = new DataOutputStream(socket.getOutputStream());
            System.out.println("Send data");
            os.writeUTF("want to connect");
            //Thread.sleep(1000);
            String message = "";
            while (is.available() > 0) {
                message=message+(is.readUTF());
            }
            if(!message.equals("success")) {
                ConnectionError = "Connection Time out";
                ConnectionErrorInfo = "The server might be busy. Please retry later";
            }
            else ConnectionEstablished = true;
            int time=0;
            while(!ConnectionEstablished){
                while (is.available() > 0) {
                    String m= is.readUTF();
                    if (m.equals("success")){
                        ConnectionEstablished = true;
                        ConnectionError = "";
                        ConnectionErrorInfo = "";
                    }
                }
            }
            while (true){
                time++;
            }
        }
        catch (UnknownHostException e) {
            ConnectionError = "UnknownHostException";
            ConnectionErrorInfo = "Please enter correct Host&Port in configure.";
        }catch (ConnectException e){
            ConnectionError = "ConnectException";
            ConnectionErrorInfo = e.getMessage();
        }/*catch (InterruptedException e){
        }*/
        catch (IOException e) {
            ConnectionError = "IOException";
            ConnectionErrorInfo = e.getMessage();
        }
        System.out.println(ConnectionError);
    }

    public String search(String key){
        if (!ConnectionEstablished){
            System.out.println(ConnectionError);
            System.out.println(ConnectionErrorInfo);
            return ConnectionErrorInfo;
        }
        if (lowercase)
            key = key.toLowerCase();
        try{
            os.writeUTF("search_"+key);
            os.flush();
            StringBuilder message = new StringBuilder();
            boolean gotmessage = false;
            while(!gotmessage) {
                while (is.available() > 0) {
                    gotmessage = true;
                    message.append(is.readUTF());
                }
            }
            return message.toString();
        }catch (IOException e){
            return e.getMessage();
        }
    }

    public String insert(String key, String meaning){
        if (!ConnectionEstablished){
            return ConnectionErrorInfo;
        }
        if (lowercase)
            key = key.toLowerCase();
        try{
            os.writeUTF("insert_"+key+"_"+meaning);
            os.flush();
            //Thread.sleep(500);
            StringBuilder message = new StringBuilder();
            boolean gotmessage = false;
            while(!gotmessage){
                while (is.available() > 0) {
                    gotmessage = true;
                    message.append(is.readUTF());
                }
            }

            return message.toString();
        }catch (IOException e){
            return e.getMessage();
        }
    }

    public String remove(String key){
        if (!ConnectionEstablished){
            return ConnectionErrorInfo;
        }
        if (lowercase)
            key = key.toLowerCase();
        try{
            os.writeUTF("remove_"+key+"");
            os.flush();

            StringBuilder message = new StringBuilder();
            boolean gotmessage = false;
            while(!gotmessage){
                while (is.available() > 0) {
                    gotmessage = true;
                    message.append(is.readUTF());
                }
            }
            return message.toString();
        }catch (IOException e){
            return e.getMessage();
        }
    }

    public void exit(){
        try {
            if (ConnectionError.equals(""))
                os.writeUTF("exit");
        } catch (IOException e) {System.exit(0); }
        System.exit(0);
    }
}