package ClientPackage;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Student Name: Rui Wang
 * Student ID: 978296
 * Subject: Distributed System
 */


public class Alert {
    public void display(String title , String message){
        Stage window = new Stage();
        window.setTitle(title);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setMinWidth(200);
        window.setMinHeight(120);
        Button Confirm = new Button("Confirm");
        Confirm.setOnAction(e -> window.close());
        Label label = new Label(message);
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label , Confirm);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
    }
}