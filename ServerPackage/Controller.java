package ServerPackage;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;

import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Student Name: Rui Wang
 * Student ID: 978296
 * Subject: Distributed System
 */

public class Controller implements Initializable {
    //private

    private Server server;
    @FXML
    private Button Exit;
    @FXML
    private TextArea connectionView;
    @FXML
    private TextField IPtodisconnect;
    @FXML
    private TextField WordNumber;
    @FXML
    private TextField ClientNumber;
    @FXML
    private TextField Port;
    @FXML
    private TextField newPort;
    @FXML
    private TextField filePath;
    @FXML
    private TextField newMax;
    @FXML
    private CheckBox fuzzySearch;
    public void initialize(URL location, ResourceBundle resources) {
        server = new Server(this);
        server.start();
        newPort.setText(Integer.toString(Constants.getPort()));
        newMax.setText(Integer.toString(Constants.getMaxConnection()));
        if(Constants.getFZ())
            fuzzySearch.setSelected(true);
    }

    public void updateConnection(HashMap<Integer,Boolean> Connection,HashMap<Integer,String>ConnectionIP, int currentUser){
        ClientNumber.setText(Integer.toString(currentUser));
        StringBuilder sb = new StringBuilder();
        for (int i: Connection.keySet()){
            sb.append(i+"     ");
            sb.append(ConnectionIP.get(i)+"     ");
            sb.append(Connection.get(i)?"Alive":"Released");
            sb.append("\n");
        }
        connectionView.setText(sb.toString());
    }

    public void disConnect(ActionEvent event){
        try{
            String ID = IPtodisconnect.getText();
            server.disconnect(Integer.valueOf(ID));
        }catch (Exception e){
            new Alert().display("Error","Not an integer.");
        }
    }

    public void exit(ActionEvent event){
        server.close();
    }

    public void setDictNumber(int n){
        WordNumber.setText(Integer.toString(n));
    }

    public void setPort(int n){
        Port.setText(Integer.toString(n));
    }

    public void setNewPort(ActionEvent event){
        String p = newPort.getText();
        try {
            int value = Integer.valueOf(p);
            if (value<0||value>=65536)
                throw new Exception();
        }catch(Exception e){
            new Alert().display("Error", "Wrong Port Number");
            return;
        }
        new Alert().display("Success", "Change saved. Restart to apply the new configure.");
    }

    public void recover(ActionEvent event){
        server.recover(filePath.getText());
    }

    public void setNewMax(ActionEvent event){
        Constants.setMaxConnection(Integer.valueOf(newMax.getText()));
    }

    public void allowFuzzySearch(ActionEvent event){
        Constants.setFuzzySearch(fuzzySearch.isSelected());
    }
}
