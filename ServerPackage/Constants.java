package ServerPackage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Student Name: Rui Wang
 * Student ID: 978296
 * Subject: Distributed System
 */

public class Constants {
    private static String Host = "localhost";
    private static int Port = 3005;
    private static int MaxConnection = 2;
    private static boolean fuzzySearch = true;
    public static String getHost(){
        return Host;
    }

    public static void setHost(String newHost){
        Host = newHost;
    }

    public static int getPort(){
        return Port;
    }

    public static void setPort(int newPort){
        Port = newPort;
    }

    public static int getMaxConnection(){ return MaxConnection; }

    public static void setMaxConnection(int newMax){ MaxConnection = newMax; }

    public static boolean getFZ(){return fuzzySearch;}

    public static void setFuzzySearch(boolean newf){fuzzySearch = newf;}

    public static void recoverFromFile(){
        try {
            Scanner StreamObject = new Scanner(new FileInputStream("constants.dat"));
            String[] record = StreamObject.nextLine().split(" ");
            setHost(record[0]);
            setPort(Integer.valueOf(record[1]));
            setMaxConnection(Integer.valueOf(record[2]));
            fuzzySearch = record[3].equals("true")?true:false;
        }catch(IOException e){}
        catch (Exception e){}
        // if there is no right record to cover, just continue to start with an empty dictionary
    }

    public static String saveAndExit(){
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream("constants.dat"));
            pw.println(Host+" "+Port+" "+MaxConnection+" "+(fuzzySearch?"true":"false"));
            pw.close();
        }catch(IOException e) {
            //If we get a saving problem, exit with code (1).
            return "Failed to save data. Please check the directory for 'dictionary.dat'";
        }
        return "Configure saved.";
    }
}
