package ServerPackage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

/**
 * Student Name: Rui Wang
 * Student ID: 978296
 * Subject: Distributed System
 */

public class Dictionary {
    private HashMap<String,String> wordMap;
    private boolean fuzzySearch = true;

    public Dictionary(boolean fuzzySearch){
        wordMap = new HashMap<>();
        this.fuzzySearch = fuzzySearch;
        recoverFromFile();
    }

    public int getNumber(){
        return wordMap.size();
    }


    public void recoverFromFile(){
        recoverFromPath("dictionary.dat");
        // if there is no right record to cover, just continue to start with an empty dictionary
    }

    public synchronized String recoverFromPath(String path){
        try {
            Scanner StreamObject = new Scanner(new FileInputStream(path));
            while(StreamObject.hasNext()){
                String[] record = StreamObject.nextLine().split("~");
                wordMap.put(record[0],record[1]);
            }return "Success.";
        }catch(IOException e){
            return e.getMessage();
        }
        catch (Exception e){
            return e.getMessage();
        }
    }

    public synchronized String saveAndExit(){
        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream("dictionary.dat"));
            Set<String> keySet = wordMap.keySet();
            for (String key: keySet) {
                pw.println(key+"~"+wordMap.get(key));
            }
            pw.close();
        }catch(IOException e) {
            //If we get a saving problem, exit with code (1).
            return "Failed to save data. Please check the directory for 'dictionary.dat'";
        }
        return "Dictionary successfully saved.";
    }

    public synchronized String search (String key){
        if(wordMap.containsKey(key))
            return wordMap.get(key);
        else if (fuzzySearch){
            String res = similarSearch(key);
            if (!res.equals(""))
                return "Not found in the dictionary. Do you mean:\n"+res;
        }
        return "The word "+key+" does not exist in the dictionary.";
    }

    public synchronized String insert (String key, String meaning){
        if(wordMap.containsKey(key)){
            return "The word "+key+" already exists in the dictionary.";
        }
        wordMap.put(key,meaning);
        return  "The word "+key+" has been successfully inserted.";
    }

    public synchronized String remove (String key){
        if(wordMap.containsKey(key)){
            wordMap.remove(key);
            return "The word "+key+" has been successfully removed.";
        }
        else if (fuzzySearch){
            String res = similarSearch(key);
            if (!res.equals(""))
                return "Not found in the dictionary. Do you mean:\n"+res;
        }
        return "The word "+key+" does not exist in the dictionary.";
    }

    public synchronized String similarSearch(String key){
        StringBuilder sb = new StringBuilder();
        for (String i:wordMap.keySet()){
            if (minDistance(i,key)==1){
                sb.append(i+"\n");
            }
        }
        return sb.toString();
    }

    private int minDistance(String word1, String word2) {
        int n = word1.length();
        int m = word2.length();
        int[][] dp = new int[n + 1][m + 1];
        for (int i = 0; i < m + 1; i++)
            dp[0][i] = i;
        for (int i = 0; i < n + 1; i++)
            dp[i][0] = i;
        for (int i = 1; i < n + 1; i++)
            for (int j = 1; j < m + 1; j++)
                    dp[i][j] = (word1.charAt(i - 1) == word2.charAt(j - 1))?dp[i - 1][j - 1]:Math.min(Math.min(dp[i - 1][j], dp[i][j - 1]), dp[i - 1][j - 1]) + 1;
        return dp[n][m];
    }
}

