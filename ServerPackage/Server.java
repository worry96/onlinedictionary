package ServerPackage;
import javax.net.ServerSocketFactory;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 * Student Name: Rui Wang
 * Student ID: 978296
 * Subject: Distributed System
 */

public class Server extends Thread{
    private Dictionary dictionary;
    private ServerSocketFactory factory;
    private Controller controller;
    private int count;
    private int currentUser;
    public HashMap<Integer,Boolean> Connection;
    public HashMap<Integer,String> ConnectionIP;
    public Server(Controller controller){
        Constants.recoverFromFile();
        dictionary = new Dictionary(Constants.getFZ());
        factory = ServerSocketFactory.getDefault();
        count = 0;
        Connection = new HashMap<>();
        ConnectionIP = new HashMap<>();
        this.controller = controller;
        controller.setDictNumber(dictionary.getNumber());
        controller.setPort(Constants.getPort());
    }

    public void updateConnection(String IP,int ID){
        Connection.put(ID,true);
        ConnectionIP.put(ID,IP);
        //count++;
        currentUser++;
        controller.updateConnection(Connection, ConnectionIP, currentUser);
    }

    @Override
    public void run(){
        try {
            ServerSocket serverSocket = new ServerSocket(Constants.getPort());
            int poolSize = Constants.getMaxConnection();
            for(int i=0;i<poolSize;i++){
                Thread thread = new Thread(()->serveClient(serverSocket));
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String disconnect(int ID){
        if(Connection.get(ID)==null){
            return "This ID is not connected.";
        }
        else{
            if(Connection.get(ID))
                currentUser--;
            Connection.put(ID, false);
            controller.updateConnection(Connection,ConnectionIP,currentUser);
            return "The connection has been released.";
        }
    }

    private void serveClient(ServerSocket serverSocket)
    {
        try {
            System.out.println("Waiting for client connection-");
            // Wait for connections.
            while(true)
            {
                int ID=-1;
                Socket client = serverSocket.accept();
                try(Socket clientSocket = client) {
                    // Input stream
                    DataInputStream input = new DataInputStream(clientSocket.getInputStream());
                    DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
                    String IP = client.getInetAddress().toString();
                    ID = count;
                    count++;
                    updateConnection(IP,ID);
                    while (Connection.get(ID)) {
                        String request = input.readUTF();
                        if(!request.equals("")){
                            if(request.equals("exit")){
                                System.out.println("client quit");
                                disconnect(ID);
                                break;
                            }
                            output.writeUTF(solveCommand(request, IP));
                        }
                    }
                    disconnect(ID);
                }catch (IOException e){
                }
                if(ID!=-1)
                    disconnect(ID);
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private String solveCommand(String Command, String IP){
        try {
            if (Command.equals("want to connect"))
                return "success";
            String[] args = Command.split("_");
            if (args[0].equals("search")) {
                return dictionary.search(args[1]);
            }
            if (args[0].equals("insert")) {
                if (args[2].equals("")) {
                    return "Please enter the meaning.";
                }
                String res = dictionary.insert(args[1], args[2]);
                controller.setDictNumber(dictionary.getNumber());
                return res;
            }
            if (args[0].equals("remove")) {
                String res = dictionary.remove(args[1]);
                controller.setDictNumber(dictionary.getNumber());
                return res;
            }
        }catch (ArrayIndexOutOfBoundsException e) {
            return e.getMessage();
        }
        return "Server: Unable to solve the command.";
    }

    public void close(){
        String res = dictionary.saveAndExit()+"\n"+Constants.saveAndExit();
        new Alert().display("Exit",res);
        System.exit(0);
    }

    public void recover(String path){
        new Alert().display("Feedback",dictionary.recoverFromPath(path));
    }
}

